<?php
/*
Plugin Name: AgentPress Listings:  Change Author
Plugin URI: https://github.com/alexgleason/agentpress-listings-authors
Description: Adds the ability to change the post author in AgentPress listings.
Version: 1.0.0
Author: Pennebaker
Author URI: http://pennebaker.com/
License: GPLv2 or later
Text Domain: agentpress-listings-authors
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

add_post_type_support( 'listing', 'author' );